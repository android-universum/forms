Android Forms
===============

[![CircleCI](https://circleci.com/bb/android-universum/forms.svg?style=shield)](https://circleci.com/bb/android-universum/forms)
[![Codecov](https://codecov.io/bb/android-universum/forms/branch/main/graph/badge.svg)](https://codecov.io/bb/android-universum/forms)
[![Codacy](https://api.codacy.com/project/badge/Grade/da6a1e39ed074735a9f767ae92b05746)](https://www.codacy.com/app/universum-studios/forms?utm_source=android-universum@bitbucket.org&amp;utm_medium=referral&amp;utm_content=android-universum/forms&amp;utm_campaign=Badge_Grade)
[![Android](https://img.shields.io/badge/android-9.0-blue.svg)](https://developer.android.com/about/versions/pie/android-9.0)
[![Robolectric](https://img.shields.io/badge/robolectric-4.3.1-blue.svg)](http://robolectric.org)
[![Android Jetpack](https://img.shields.io/badge/Android-Jetpack-brightgreen.svg)](https://developer.android.com/jetpack)

## ! OBSOLETE ! ##

**> This project has become obsolete and will be no longer maintained. <**

---

Set of components for the Android platform that may be used to check a simple form in application's Ui.

## Download ##

Download the latest **[release](https://bitbucket.org/android-universum/forms/addon/pipelines/deployments "Deployments page")** or **add as dependency** in your project via:

### Gradle ###

    implementation "universum.studios.android:forms:${DESIRED_VERSION}@aar"

## Modules ##

This library may be used via **separate [modules](https://bitbucket.org/android-universum/forms/src/main/MODULES.md)**
in order to depend only on desired _parts of the library's code base_ what ultimately results in **fewer dependencies**.

## Compatibility ##

Supported down to the **Android [API Level 14](http://developer.android.com/about/versions/android-4.0.html "See API highlights")**.

### Dependencies ###

- [`androidx.annotation:annotation`](https://developer.android.com/jetpack/androidx)
- [`androidx.legacy:legacy-support-v4`](https://developer.android.com/jetpack/androidx)
- [`androidx.appcompat:appcompat`](https://developer.android.com/jetpack/androidx)
- [`androidx.recyclerview:recyclerview`](https://developer.android.com/jetpack/androidx)
- [`androidx.vectordrawable:vectordrawable`](https://developer.android.com/jetpack/androidx)
- [`universum.studios.android:ui`](https://bitbucket.org/android-universum/ui)

## [License](https://bitbucket.org/android-universum/forms/src/main/LICENSE.md) ##

**Copyright 2020 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.
     
See the License for the specific language governing permissions and limitations under the License.