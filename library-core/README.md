Forms-Core
===============

This module contains core elements and interfaces that may be used for forms validation.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aforms/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aforms/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:forms-core:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [FormComponent](https://bitbucket.org/android-universum/forms/src/main/library-core/src/main/java/universum/studios/android/form/FormComponent.java)
- [FormValidator](https://bitbucket.org/android-universum/forms/src/main/library-core/src/main/java/universum/studios/android/form/FormValidator.java)