/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.form;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A FormComponent is a general abstraction for 'object that is part of a form', more exactly an object
 * which wraps a view that is part of a view hierarchy representing a form of which input data need
 * to be validated.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param <V> Type of the view specific for this form component.
 * @param <I> Type of the input that can be presented within the component view.
 */
public interface FormComponent<V extends View, I> {

	/**
	 * Attaches this form component to the given <var>validator</var>.
	 *
	 * @param validator An instance of FormValidator with which is this form component associated.
	 */
	void attachToValidator(@NonNull FormValidator validator);

	/**
	 * Checks whether the current value of this form component has changed since its last validation.
	 * <p>
	 * Basically, whenever this returns {@code true} this component should be validated, otherwise
	 * there is no need given the fact that its value did not change.
	 *
	 * @return {@code True} if the current value is different from the one that has been last time
	 * validated via {@link #validate()}, {@code false} otherwise.
	 */
	boolean hasInputChanged();

	/**
	 * Runs validation process of this form component.
	 *
	 * @return {@code True} if the current state of this form component's view is valid,
	 * {@code false} otherwise.
	 */
	boolean validate();

	/**
	 * Returns the id of this component.
	 *
	 * @return This form component's id.
	 */
	int getId();

	/**
	 * Returns the view attached to this form component.
	 *
	 * @return This form component's view.
	 */
	@Nullable V getView();

	/**
	 * Returns the current input presented by an attached view of this form component. Type of input
	 * is determined by a specific form component type and may vary.
	 *
	 * @return Current input of this form component presented by its view.
	 */
	@NonNull I getInput();

	/**
	 * Sets an error for this form component.
	 * <p>
	 * <b>Note</b>, that a specific implementations can ignore this call and also depends on the specific
	 * implementation how the requested error parameter will be shown.
	 *
	 * @param errorMessage The desired error message.
	 *
	 * @see #clearError()
	 */
	void setError(@NonNull CharSequence errorMessage);

	/**
	 * Checks whether this form component has error specified or not.
	 *
	 * @return {@code True} if error has been specified via {@link #setError(CharSequence)},
	 * {@code false} if not or has been cleared via {@link #clearError}.
	 */
	boolean hasError();

	/**
	 * Clears the current error (if any) of this form component specified via {@link #setError(CharSequence)}.
	 */
	void clearError();

	/**
	 * Looks for a form component with the requested <var>id</var>.
	 *
	 * @param componentId Id of the desired component to find.
	 * @return An instance of the requested form component, {@code null} if component with such a
	 * <var>componentId</var> was not found.
	 */
	@Nullable FormComponent findComponent(int componentId);
}