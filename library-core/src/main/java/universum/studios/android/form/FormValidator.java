/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.form;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IdRes;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * A FormValidator can be used to simplify validation process of a form with user inputs like
 * {@link EditText EditTexts} or {@link CompoundButton CompoundButtons} (CheckBox, RadioButton, ...).
 * <p>
 * Validation process can be launched via {@link #runValidation()}. FormValidator runs validation
 * process on its current assigned {@link FormComponent FormComponents} that can be assigned to a
 * validator instance via following methods:
 * <ul>
 * <li>{@link #assignComponent(FormComponent)}</li>
 * <li>{@link #assignComponents(FormComponent[])}</li>
 * <li>{@link #assignComponentViews(int...)}</li>
 * </ul>
 * Based on the validation mode (described in the section below), the validator iterates these
 * components and invokes {@link FormComponent#validate()} upon each of them and notifies
 * {@link FormValidator.OnFormListener OnFormListener} that can be specified via
 * {@link #setOnFormListener(FormValidator.OnFormListener)} about occurred errors and about validation
 * result.
 * <p>
 * All current assigned form components can be obtained via {@link #getComponents()}. To obtain a
 * single component, call {@link #getComponent(int)} with id of that component.
 *
 * <h3>Validation modes</h3>
 * FormValidator supports following validation methods that are used to determine the validation flow:
 * <ul>
 * <li>
 * {@link #VALIDATION_MODE_BULK}
 * <p>
 * Assigned form components are validated regardless of occurred errors
 * ({@link FormValidator.OnFormListener#onValidationError(FormValidator, FormComponent) OnFormListener.onValidationError(FormValidator, FormComponent)})
 * and validation result is notified via
 * {@link FormValidator.OnFormListener#onValidationResult(FormValidator, boolean) OnFormListener.onValidationResult(FormValidator, boolean)}
 * after all form components has been validated.
 * </li>
 * <li>
 * {@link #VALIDATION_MODE_SINGLE}
 * <p>
 * Assigned form components are validated until the first error occurs. Then the validation process
 * is stopped, {@code OnFormListener.onValidationError(...)} is fired followed by {@code OnFormListener.onValidationResult(...)}
 * with validation result set to {@code false}.
 * </li>
 * </ul>
 * The desired validation mode can be specified via {@link #setValidationMode(int)}. The default one
 * is {@link #VALIDATION_MODE_BULK}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class FormValidator {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	private static final String TAG = "FormValidator";

	/**
	 * Mode used to validate <b>all</b> currently assigned {@link BaseFormComponent}s. If some validation
	 * error occurs, {@link OnFormListener#onValidationError(FormValidator, FormComponent)} will be
	 * fired with that particular component, but validation process will continue and
	 * {@link OnFormListener#onValidationResult(FormValidator, boolean)} will be fired after all
	 * form components has been validated with either {@code true} or {@code false} validation result.
	 */
	public static final int VALIDATION_MODE_BULK = 0x01;

	/**
	 * Unlike {@link #VALIDATION_MODE_BULK}, when this mode is used, validation will be running
	 * until some validation error occurs. At that time {@link OnFormListener#onValidationError(FormValidator, FormComponent)}
	 * will be fired with that particular component and {@link OnFormListener#onValidationResult(FormValidator, boolean)}
	 * will be fired with {@code false} as validation result and whole validation process stops.
	 */
	public static final int VALIDATION_MODE_SINGLE = 0x02;

	/**
	 * Defines an annotation for determining set of allowed validation modes for {@link #setValidationMode(int)}
	 * method.
	 */
	@IntDef({VALIDATION_MODE_BULK, VALIDATION_MODE_SINGLE})
	@Retention(RetentionPolicy.SOURCE)
	public @interface ValidationMode {}

	/*
	 * Interface ===================================================================================
	 */

	/**
	 * Simple listener to receive callbacks when some validation errors occurs during form validation
	 * process and also callback with the result of validation process.
	 *
	 * @author Martin Albedinsky
	 */
	public interface OnFormListener {

		/**
		 * Invoked during validation process, when the passed form <var>component</var> returns
		 * {@code false} from {@link BaseFormComponent#validate()}.
		 *
		 * @param validator The form validator within which has error occurred.
		 * @param component Form component of which view does not passed current validation requirements.
		 * @see #onValidationResult(FormValidator, boolean)
		 */
		void onValidationError(@NonNull FormValidator validator, @NonNull FormComponent component);

		/**
		 * Invoked at the end of each validation process. End of validation process is determined by
		 * the current form validator validation mode. See {@link #VALIDATION_MODE_BULK} and
		 * {@link #VALIDATION_MODE_SINGLE} for more info.
		 *
		 * @param validator The form validator within which has been validation finished.
		 * @param passed    {@code True} if validation succeeded, {@code false} if at least one
		 *                  form component returns {@code false} from {@link BaseFormComponent#validate()}
		 *                  during validation process.
		 */
		void onValidationResult(@NonNull FormValidator validator, boolean passed);
	}

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Simple on click listener for submit view to run validation when its
	 * {@link View.OnClickListener#onClick(View)} is fired.
	 */
	private final View.OnClickListener SUBMIT_LISTENER = new View.OnClickListener() {

		/**
		 */
		@Override public void onClick(@NonNull final View view) {
			runValidation();
		}
	};

	/**
	 * Array of all assigned form components.
	 */
	private final SparseArray<FormComponent> components = new SparseArray<>();

	/**
	 * Context in which is this validator used.
	 */
	protected final Context context;

	/**
	 * View in which are placed all form component views.
	 */
	protected final View formView;

	/**
	 * Current validation mode. Default set to {@link #VALIDATION_MODE_BULK}.
	 */
	private int validationMode = VALIDATION_MODE_BULK;

	/**
	 * Current OnFormListener callback which can receive validation errors and validation result.
	 */
	private OnFormListener listener;

	/**
	 * Array of ids of views which should be obtained from the current context wrapper root view
	 * and assigned as FormComponents to this validator.
	 */
	private int[] viewIds;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link FormValidator#FormValidator(View, int)} with {@link #VALIDATION_MODE_BULK}
	 * validation mode.
	 */
	public FormValidator(@NonNull final View formView) {
		this(formView, VALIDATION_MODE_BULK);
	}

	/**
	 * Creates a new instance of FormValidator for the given <var>formView</var> with the specified
	 * validation <var>mode</var>.
	 *
	 * @param formView The view in which are placed all form component views.
	 * @param mode     The validation mode flag. One of {@link #VALIDATION_MODE_BULK} or {@link #VALIDATION_MODE_SINGLE}.
	 */
	public FormValidator(@NonNull final View formView, @ValidationMode final int mode) {
		this.context = formView.getContext();
		this.formView = formView;
		this.validationMode = mode;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Assigns the given form components to this validator.
	 *
	 * @param components Form components which should be validated by this validator.
	 * @return This validator to allow methods chaining.
	 */
	public FormValidator assignComponents(@NonNull final FormComponent... components) {
		if (components.length > 0) {
			for (final FormComponent component : components) {
				assignComponent(component);
			}
		}
		return this;
	}

	/**
	 * Assigns the given form component to this validator.
	 *
	 * @param component Form component which should be validated by this validator.
	 * @return This validator to allow methods chaining.
	 */
	public FormValidator assignComponent(@NonNull final FormComponent component) {
		final int componentId = component.getId();
		if (components.indexOfKey(componentId) < 0) {
			component.attachToValidator(this);
			components.put(componentId, component);
		}
		return this;
	}

	/**
	 * Assigns the set of ids of views which should be validated by this validator.
	 *
	 * @param viewIds The set of ids of desired views from the current context's root view for which
	 *                should be auto-created FormComponents and assigned to this validator. Type of
	 *                each created form component, will be resolved from the type of view.
	 * @return This validator to allow methods chaining.
	 */
	public FormValidator assignComponentViews(final int... viewIds) {
		this.viewIds = viewIds;
		return this;
	}

	/**
	 * Runs validation process upon the current set of form components.
	 * <p>
	 * <b>Note</b>, that all current errors will be cleared via {@link #clearErrors()} before
	 * validation process starts.
	 *
	 * @return {@code True} if validation was performed, {@code false} otherwise. This will
	 * return {@code true} always if there are some form components to validate.
	 */
	public boolean runValidation() {
		if (viewIds != null) {
			this.assignComponentsForViews();
			this.viewIds = null;
		}
		final int n = components.size();
		if (n == 0) {
			return false;
		}
		boolean passed = true;
		for (int i = 0; i < n; i++) {
			final FormComponent component = components.get(components.keyAt(i));
			if (component.hasInputChanged()) {
				if (component.validate()) {
					component.clearError();
				} else {
					this.notifyValidationError(component);
					if (validationMode == VALIDATION_MODE_SINGLE) {
						this.notifyValidationResult(false);
						return true;
					}
					passed = false;
				}
			} else if (component.hasError()) {
				passed = false;
			}
		}
		this.notifyValidationResult(passed);
		return true;
	}

	/**
	 * Clears the current errors of all assigned components.
	 *
	 * @see BaseFormComponent#clearError()
	 */
	public void clearErrors() {
		final int n = components.size();
		for (int i = 0; i < n; i++) {
			components.get(components.keyAt(i)).clearError();
		}
	}

	/**
	 * Dispatches {@link OnFormListener#onValidationError(FormValidator, FormComponent)} callback to
	 * the current form listener with the passed <var>component</var>.
	 *
	 * @param component Form component of which view does not passed current validation requirements.
	 */
	private void notifyValidationError(final FormComponent component) {
		if (listener != null) listener.onValidationError(this, component);
	}

	/**
	 * Dispatches {@link OnFormListener#onValidationResult(FormValidator, boolean)} callback to the
	 * current form listener with the passed validation result.
	 *
	 * @param passed {@code True} if validation succeed, {@code false} otherwise.
	 */
	private void notifyValidationResult(final boolean passed) {
		if (listener != null) listener.onValidationResult(this, passed);
	}

	/**
	 * Assigns form components for the current view ids. Type of each form component to assign, will
	 * be resolved from the type of view.
	 */
	private void assignComponentsForViews() {
		for (final int viewId : viewIds) {
			final View view = findViewById(viewId);
			if (view == null || !onAssignComponentForView(view)) {
				Log.e(TAG, "Failed to assign form component for view(" + view + ") with id(" + viewId + ").");
			}
		}
	}

	/**
	 * Invoked to assign a new form component to this validator for the specified <var>componentView</var>.
	 * <p>
	 * <b>Note</b>, that this is invoked in case when there was supplied a set of ids of views to this
	 * validator via {@link #assignComponentViews(int...)} for which should be created and assigned
	 * form components. This will be invoked from {@link #runValidation()} whenever there are view
	 * ids that has not been processed yet.
	 * <p>
	 * This implementation does nothing.
	 *
	 * @param componentView The view for which to assign new form component that will be validated
	 *                      by this validator.
	 * @return {@code True} to indicate that component for the view has been successfully assigned,
	 * {@code false} otherwise.
	 */
	protected boolean onAssignComponentForView(@NonNull View componentView) {
		return false;
	}

	/**
	 * Returns the context obtained from the context wrapper during initialization of this validator.
	 *
	 * @return Valid context.
	 */
	@Nullable public Context getContext() {
		return context;
	}

	/**
	 * Same as {@link #setSubmitView(View)} for id of the submit view that will be searched for
	 * in the form view.
	 *
	 * @param id An id of the desired submit view.
	 */
	public void setSubmitView(final int id) {
		final View submitView = formView.findViewById(id);
		if (submitView != null) setSubmitView(submitView);
	}

	/**
	 * Sets the submit view for this validator.
	 * <p>
	 * <b>Note</b>, that this will attach to the given <var>submitView</var> an instance of
	 * {@link View.OnClickListener}, so any previous OnClickListener attached to this view will be
	 * replaced.
	 *
	 * @param submitView The view, on which click should be validation executed.
	 */
	public void setSubmitView(@NonNull final View submitView) {
		submitView.setOnClickListener(SUBMIT_LISTENER);
	}

	/**
	 * Registers a callback to be invoked when some validation error occurs during form validation
	 * process or when validation process is finished with the validation result.
	 *
	 * @param listener Listener callback to register. May be {@code null} to remove the current one.
	 */
	public void setOnFormListener(@Nullable final OnFormListener listener) {
		this.listener = listener;
	}

	/**
	 * Sets validation mode for this validator.
	 *
	 * @param mode One of {@link #VALIDATION_MODE_BULK} or {@link #VALIDATION_MODE_SINGLE}.
	 */
	public void setValidationMode(@ValidationMode final int mode) {
		this.validationMode = mode;
	}

	/**
	 * Returns the current validation mode of this validator.
	 *
	 * @return One of {@link #VALIDATION_MODE_BULK} or {@link #VALIDATION_MODE_SINGLE}.
	 */
	@ValidationMode public int getValidationMode() {
		return validationMode;
	}

	/**
	 * Returns the set of current form components of this validator.
	 *
	 * @return All assigned form components.
	 */
	@NonNull public SparseArray<FormComponent> getComponents() {
		return components;
	}

	/**
	 * Returns an instance of FormComponent with the requested id. Unlike {@link #findComponent(int)},
	 * this will check only at the top of current components.
	 *
	 * @param componentId An id of the desired form component to obtain.
	 * @return An instance of requested form component with the passed id or {@code null} if there
	 * is no such a form component assigned to this validator.
	 */
	@Nullable public FormComponent getComponent(final int componentId) {
		return components.size() > 0 ? components.get(componentId) : null;
	}

	/**
	 * Returns an instance of FormComponent with the requested id. This will traverse whole hierarchy
	 * of assigned components/component groups, to find that particular form component.
	 *
	 * @param componentId An id of the desired form component to find.
	 * @return An instance of requested form component with the passed id or {@code null} if there
	 * is no such a form component assigned to this validator or any of assigned form component groups
	 * does not contain form component with such an id.
	 */
	@Nullable public FormComponent findComponent(final int componentId) {
		final int n = components.size();
		for (int i = 0; i < n; i++) {
			final FormComponent component = components.get(components.keyAt(i)).findComponent(componentId);
			if (component != null) {
				return component;
			}
		}
		return null;
	}

	/**
	 * Searches for a form component view within the form view specified during initialization of
	 * this validator using the specified <var>id</var>.
	 *
	 * @param id Id of the desired form view to find.
	 */
	final View findViewById(@IdRes final int id) {
		return formView.findViewById(id);
	}

	/*
	 * Inner classes ===============================================================================
	 */
}