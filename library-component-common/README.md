Forms-Component-Common
===============

This module contains common form components that use widgets like `Checkable` or `EditText` as
theirs input views.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aforms/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aforms/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:forms-component-common:${DESIRED_VERSION}@aar"

_depends on:_
[forms-core](https://bitbucket.org/android-universum/forms/src/main/library-core),
[forms-component-base](https://bitbucket.org/android-universum/forms/src/main/library-component-base)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [FormCheckable](https://bitbucket.org/android-universum/forms/src/main/library-component-common/src/main/java/universum/studios/android/form/FormCheckable.java)
- [FormEditable](https://bitbucket.org/android-universum/forms/src/main/library-component-common/src/main/java/universum/studios/android/form/FormEditable.java)