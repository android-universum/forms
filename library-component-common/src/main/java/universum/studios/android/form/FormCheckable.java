/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.form;

import android.content.Context;
import android.view.View;
import android.widget.Checkable;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;

/**
 * A {@link BaseFormComponent} implementation that wraps form checkable view like
 * {@link android.widget.CheckBox CheckBox} or {@link android.widget.RadioButton RadioButton} into
 * component that can be validated by {@link universum.studios.android.form.FormValidator} when
 * validating a form in which is such a view presented.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see FormEditable
 */
public class FormCheckable<V extends View & Checkable> extends BaseFormComponent<V, Boolean> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "FormCheckable";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Flag indicating, whether the currently validating checkable view should be checked to pass
	 * validation process or not.
	 */
	private final boolean requiresChecked;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #FormCheckable(int, boolean)} with <var>requiresChecked</var> parameter set to
	 * {@code true}.
	 */
	public FormCheckable(@IdRes final int id) {
		this(id, true);
	}

	/**
	 * Creates a new instance of FormCheckable component for a checkable view with the specified
	 * <var>id</var>.
	 *
	 * @param id              Id used to find the checkable view within the form view hierarchy of
	 *                        the associated {@link FormValidator} via {@link FormValidator#findViewById(int)}.
	 * @param requiresChecked Flag to be compared to the checked state of the checkable view during
	 *                        the validation process.
	 */
	public FormCheckable(@IdRes final int id, final boolean requiresChecked) {
		super(id, null);
		this.requiresChecked = requiresChecked;
	}

	/**
	 * Same as {@link #FormCheckable(View, boolean)} with <var>requiresChecked</var> parameter
	 * set to {@code true}.
	 */
	public FormCheckable(@NonNull final V checkable) {
		this(checkable, true);
	}

	/**
	 * Creates a new instance of FormCheckable component for the given <var>checkable</var> view.
	 *
	 * @param checkable       Checkable view that should be validated.
	 * @param requiresChecked Flag to be compared to the checked state of the checkable view during
	 *                        the validation process.
	 * @see BaseFormComponent#BaseFormComponent(int, View)
	 */
	public FormCheckable(@NonNull final V checkable, final boolean requiresChecked) {
		super(checkable.getId(), checkable);
		this.requiresChecked = requiresChecked;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override protected boolean onValidate(@NonNull final Boolean input) {
		return requiresChecked && input;
	}

	/**
	 * Checks, whether the compound view of this component is checked or not.
	 *
	 * @return {@code True} if checkable view is checked, {@code false} otherwise.
	 */
	public boolean isChecked() {
		return getInput();
	}

	/**
	 */
	@Override @NonNull public Boolean getInput() {
		return view != null && view.isChecked();
	}

	/**
	 */
	@Override public void setError(@NonNull final CharSequence errorMessage) {
		if (validator != null) {
			final Context context = validator.getContext();
			if (context != null) {
				Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
			}
		}
	}

	/**
	 * This implementation does nothing.
	 * <p>
	 * <b>Note</b>, that this form component cannot have error specified as it just shows a toast
	 * whenever {@link #setError(CharSequence)} is called.
	 */
	@Override public void clearError() {
		super.clearError();
	}

	/*
	 * Inner classes ===============================================================================
	 */
}