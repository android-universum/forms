/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.form;

import android.content.res.Resources;
import android.view.View;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

/**
 * A {@link FormComponent} implementation that can be used as base for specific form components
 * implementations.
 * <p>
 * This 'wrapper' is used by {@link FormValidator} that can be used to validate these input data.
 * The FormValidator will call {@link #validate()} on each of its attached form components during the
 * validation process. If some error occurs, the FormValidator will fire error callback and if needed
 * a desired error can be set to the error component via {@link #setError(CharSequence)}. The form
 * validator to which is the form component currently attached can be accessed via {@link #getValidator()}.
 * <p>
 * Each form component must be created with its view of which validation will be then handled by that
 * component. There are two base constructors for this purpose:
 * <ul>
 * <li>
 * {@link #BaseFormComponent(int)}
 * <p>
 * Use this constructor when you want to be a form view attached automatically by its form component
 * when needed using its id.
 * </li>
 * <li>
 * {@link #BaseFormComponent(View)}
 * <p>
 * Use this constructor when you have already access to a form view.
 * </li>
 * </ul>
 * Wrapped form view can be accessed via {@link #getView()}.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param <V> Type of the view specific for this form component.
 * @param <I> Type of the input that can be presented within the component view.
 */
public abstract class BaseFormComponent<V extends View, I> implements FormComponent<V, I> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "FormComponent";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Identifier of this form component. Should be unique to correctly identify different form components.
	 */
	final int id;

	/**
	 * Form validator to which is this form component attached. Form component should be always attached
	 * to some validator.
	 */
	FormValidator validator;

	/**
	 * View for which is this form component created.
	 */
	V view;

	/**
	 * Input value that has been last time validated.
	 */
	I lastValidatedInput;

	/**
	 * Application resources.
	 */
	private Resources resources;

	/**
	 * Flag indicating whether this form component has error set or not.
	 */
	private boolean hasError;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of base FormComponent.
	 * <p>
	 * The specified <var>id</var> will be used to find a view for this new form component via
	 * {@link FormValidator#findViewById(int)}  when appropriate.
	 *
	 * @param id The id for the new form component. Should be unique to correctly identify different
	 *           components within {@link FormValidator} or {@link FormComponentsGroup}.
	 */
	protected BaseFormComponent(@IntRange(from = 0) final int id) {
		this(id, null);
	}

	/**
	 * Creates a new instance of base FormComponent.
	 * <p>
	 * Id of the specified <var>view</var> will be assigned also as id of this new form component.
	 *
	 * @param view The view for which is the new form component created.
	 */
	protected BaseFormComponent(@NonNull final V view) {
		this(view.getId(), view);
	}

	/**
	 * Creates a new instance of base FormComponent.
	 *
	 * @param id   The id for the new form component. Should be unique to correctly identify different
	 *             components within {@link FormValidator} or {@link FormComponentsGroup}.
	 * @param view The view for which is the new form component created. If {@code null} the specified
	 *             id will be used to find such view via {@link FormValidator#findViewById(int)}
	 *             when appropriate.
	 */
	protected BaseFormComponent(@IntRange(from = 0) final int id, @Nullable final V view) {
		this.view = view;
		this.id = id;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@SuppressWarnings("ConstantConditions")
	@Override public void attachToValidator(@NonNull final FormValidator validator) {
		this.validator = validator;
		this.resources = validator.getContext().getResources();
		onAttachedToValidator(validator);
	}

	/**
	 * Invoked whenever this form component is assigned to the passed <var>validator</var>.
	 *
	 * @param validator An instance of FormValidator to which was this form component newly assigned.
	 */
	protected void onAttachedToValidator(@NonNull FormValidator validator) {
		// Inheritance hierarchies may perform here theirs specific configuration associated with
		// the attached validator.
	}

	/**
	 */
	@Override public boolean hasInputChanged() {
		final I input = getInput();
		return lastValidatedInput == null || !lastValidatedInput.equals(input);
	}

	/**
	 */
	@Override public boolean validate() {
		this.ensureView();
		resetCurrentState();
		return onValidate(lastValidatedInput = getInput());
	}

	/**
	 * Ensures that the view of this form component is initialized.
	 */
	private void ensureView() {
		if (view == null && id > 0) attachComponentView(id);
	}

	/**
	 * Called to reset the current state of this form component. This is always called from {@link #validate()}
	 * before {@link #onValidate(Object)} is called.
	 */
	protected void resetCurrentState() {
		// Inheritance hierarchies may reset theirs current state here.
	}

	/**
	 * Returns the input that has been last time validated via {@link #validate()}.
	 *
	 * @return Last validated input or {@code null} if no validation has been performed yet.
	 */
	@Nullable protected final I getLastValidatedInput() {
		return lastValidatedInput;
	}

	/**
	 * Invoked whenever {@link #validate()} is called. Note, that there will be {@link #resetCurrentState()}
	 * always called before this.
	 *
	 * @param input Current input of this form component to be validated.
	 * @return Should return {@code true} if the current form component's view state passed
	 * validation requirements, {@code false} otherwise.
	 *
	 * @see #getInput()
	 */
	protected abstract boolean onValidate(@NonNull I input);

	/**
	 * Returns flag indicating whether this form component is attached to FormValidator ot not.
	 *
	 * @return {@code True} if this form component is attached to FormValidator, {@code false}
	 * otherwise.
	 * @see #getValidator()
	 */
	public final boolean isAttachedToValidator() {
		return validator != null;
	}

	/**
	 * Returns the instance of {@link FormValidator} to which is this form component currently attached.
	 *
	 * @return An instance of FormValidator or {@code null} if this form component is not attached
	 * to any FormValidator.
	 * @see #isAttachedToValidator()
	 */
	@Nullable public final FormValidator getValidator() {
		return validator;
	}

	/**
	 */
	@Override public final int getId() {
		return id;
	}

	/**
	 */
	@Override @Nullable public V getView() {
		this.ensureView();
		return view;
	}

	/**
	 */
	@Override public void clearError() {
		setHasError(false);
	}

	/**
	 * Sets a boolean flag indicating whether this form component has error set or not.
	 *
	 * @param hasError {@code True} if error is set, {@code false} otherwise.
	 * @see #hasError()
	 */
	protected final void setHasError(final boolean hasError) {
		this.hasError = hasError;
	}

	/**
	 */
	@Override public boolean hasError() {
		return hasError;
	}

	/**
	 * Returns the resources obtained form the current attached FormValidator.
	 *
	 * @return An instance of application resources or {@code null} if this form component isn't
	 * attached to any FormValidator.
	 */
	@Nullable protected final Resources getResources() {
		return resources;
	}

	/**
	 * Wrapped {@link Resources#getString(int)} (int)} on resources obtained from the current attached
	 * FormValidator.
	 *
	 * @return Can return empty string if this form component isn't attached to any FormValidator, so
	 * no resources are available at this time.
	 */
	@NonNull protected final String obtainString(@StringRes final int resId) {
		return resources == null ? "" : resources.getString(resId);
	}

	/**
	 * Wrapped {@link Resources#getText(int)} on resources obtained from the current attached FormValidator.
	 *
	 * @return Can return empty text if this form component isn't attached to any FormValidator, so
	 * no resources are available at this time.
	 */
	@NonNull protected final CharSequence obtainText(@StringRes final int resId) {
		return resources == null ? "" : resources.getText(resId);
	}

	/**
	 */
	@Override @Nullable public final FormComponent findComponent(final int componentId) {
		return componentId > 0 ? findComponentTraversal(componentId) : null;
	}

	/**
	 * Called immediately after {@link #findComponent(int)} if the passed <var>componentId</var> is
	 * valid. This can be useful if this component represents a group of form components.
	 *
	 * @param id Id of the desired component to find.
	 * @return This component if the passed <var>id</var> is same as this component's id, {@code null}
	 * otherwise.
	 */
	@Nullable protected FormComponent findComponentTraversal(final int id) {
		return this.id == id ? this : null;
	}

	/**
	 * Called to find a view with the specified <var>viewId</var> within the current context root view
	 * hierarchy to be attached to this form component.
	 *
	 * @param viewId Id of the view for which was this form component created.
	 */
	@SuppressWarnings("unchecked")
	private void attachComponentView(final int viewId) {
		if ((view = (V) validator.findViewById(viewId)) == null) {
			throw new IllegalStateException(
					"Failed to attach component view with id(" + viewId + "). " +
							"View not found in the current root view hierarchy."
			);
		}
		onComponentViewAttached(view);
	}

	/**
	 * Invoked only once, when there was {@link #validate()} called and the current component view is
	 * {@code null} and here passed <var>view</var> was successfully obtained from the current
	 * context root view hierarchy.
	 *
	 * @param view The view obtained from the root view of the current context by the <var>id</var>
	 *             provided during initialization of this component.
	 */
	protected void onComponentViewAttached(@NonNull V view) {
		// Inheritance hierarchies may perform here configuration of the attached view.
	}

	/*
	 * Inner classes ===============================================================================
	 */
}