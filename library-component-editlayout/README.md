Forms-Component-EditLayout
===============

This module contains a form component that uses `EditLayout` widget as its input view.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aforms/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aforms/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:forms-component-editlayout:${DESIRED_VERSION}@aar"

_depends on:_
[forms-core](https://bitbucket.org/android-universum/forms/src/main/library-core),
[forms-component-base](https://bitbucket.org/android-universum/forms/src/main/library-component-base)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [FormEditableLayout](https://bitbucket.org/android-universum/forms/src/main/library-component-editlayout/src/main/java/universum/studios/android/form/FormEditableLayout.java)