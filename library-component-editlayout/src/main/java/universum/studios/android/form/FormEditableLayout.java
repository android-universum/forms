/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.form;

import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.View;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import universum.studios.android.ui.widget.EditLayout;

/**
 * A {@link BaseFormComponent} implementation which wraps form {@link EditLayout} into component that
 * can be validated by {@link FormValidator} when validating a form in which is such a layout presented.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class FormEditableLayout extends BaseFormComponent<EditLayout, CharSequence> {

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "FormEditableLayout";

	/**
	 * Regular expression to check for <b>not</b> empty text.
	 */
	public static final String NOT_EMPTY_REG_EXP = "^(.+)$";

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Matcher used to check input of this form component's view.
	 */
	private Matcher editMatcher;

	/**
	 * Resource id of the regular expression for {@link #editMatcher}.
	 */
	private final int regExpResId;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Same as {@link #FormEditableLayout(int, String)} with {@link FormEditable#NOT_EMPTY_REG_EXP}
	 * for <var>regExp</var> parameter.
	 */
	public FormEditableLayout(@IdRes final int id) {
		this(id, NOT_EMPTY_REG_EXP);
	}

	/**
	 * Creates a new instance of FormEditableLayout component for an editable input layout with the
	 * specified <var>id</var> and the specified regular expression.
	 *
	 * @param id     Id used to find the editable layout within the form view hierarchy of
	 *               the associated {@link FormValidator} via {@link FormValidator#findViewById(int)}.
	 * @param regExp The desired regular expression that is used to validate editable
	 *               value obtained from the editable view during the validation process.
	 */
	public FormEditableLayout(@IdRes final int id, @NonNull final String regExp) {
		super(id, null);
		this.editMatcher = Pattern.compile(regExp).matcher("");
		this.regExpResId = -1;
	}

	/**
	 * Creates a new instance of FormEditableLayout component for an an editable input layout with
	 * the specified <var>id</var>.
	 *
	 * @param id          Id used to find the editable layout within the form view hierarchy of
	 *                    the associated {@link FormValidator} via {@link FormValidator#findViewById(int)}.
	 * @param regExpResId Resource id of the desired regular expression that is used to validate editable
	 *                    value obtained from the editable view during the validation process.
	 */
	public FormEditableLayout(@IdRes final int id, @StringRes final int regExpResId) {
		super(id, null);
		this.regExpResId = regExpResId;
	}

	/**
	 * Same as {@link #FormEditableLayout(EditLayout, String)} with {@link FormEditable#NOT_EMPTY_REG_EXP}
	 * for <var>regExp</var> parameter.
	 */
	public FormEditableLayout(@NonNull final EditLayout editable) {
		this(editable, NOT_EMPTY_REG_EXP);
	}

	/**
	 * Same as {@link #FormEditableLayout(EditLayout, String)} for regular expression resource id.
	 *
	 * @param regExpResId Resource id of the desired regular expression.
	 */
	public FormEditableLayout(@NonNull final EditLayout editable, @StringRes final int regExpResId) {
		this(editable, editable.getResources().getString(regExpResId));
	}

	/**
	 * Creates a new instance of FormEditable component for the given <var>editable</var> view with
	 * the specified regular expression.
	 *
	 * @param editable Editable input layout, which should be validated.
	 * @param regExp   The desired regular expression used to validate editable value obtained from
	 *                 the <var>editable</var> view during the validation process.
	 * @see BaseFormComponent#BaseFormComponent(int, View)
	 */
	public FormEditableLayout(@NonNull final EditLayout editable, @NonNull final String regExp) {
		super(editable.getId(), editable);
		this.editMatcher = Pattern.compile(regExp).matcher("");
		this.regExpResId = -1;
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 */
	@Override public boolean hasInputChanged() {
		return lastValidatedInput == null || !TextUtils.equals(lastValidatedInput, getInput());
	}

	/**
	 */
	@Override protected boolean onValidate(@NonNull final CharSequence input) {
		return editMatcher.reset(input).matches();
	}

	/**
	 */
	@Override @NonNull public CharSequence getInput() {
		final Editable editable = getEditableInput();
		return editable == null ? "" : new SpannableStringBuilder(editable);
	}

	/**
	 * Returns the current editable value of the attached text field view.
	 *
	 * @return Current editable, or {@code null} if view of this component is {@code null}.
	 */
	@Nullable public Editable getEditableInput() {
		return view == null ? null : view.getEditableText();
	}

	/**
	 */
	@Override public void setError(@NonNull final CharSequence errorMessage) {
		setHasError(true);
		if (view != null) view.setError(errorMessage);
	}

	/**
	 */
	@Override public void clearError() {
		super.clearError();
		if (view != null) view.clearError();
	}

	/**
	 */
	@Override protected void onAttachedToValidator(@NonNull final FormValidator validator) {
		super.onAttachedToValidator(validator);
		if (regExpResId > 0) {
			this.editMatcher = Pattern.compile(obtainString(regExpResId)).matcher("");
		}
	}

	/*
	 * Inner classes ===============================================================================
	 */
}