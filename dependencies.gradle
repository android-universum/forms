// Defines dependencies used across the library project.
def versions = [
        library    : "${config.library.version.production}",
        androidx   : [test: [espresso: '3.2.0']],
        butterknife: '10.2.1',
        test       : [
                junit      : '4.13',
                mockito    : '3.3.0',
                robolectric: '4.3.1'
        ]
]

def libraryArtifactBaseId = "${config.pom.groupId}:${config.pom.artifactId}"
def library = [
        core               : "${libraryArtifactBaseId}-core:${versions.library}@aar",
        component          : "${libraryArtifactBaseId}-component:${versions.library}@aar",
        componentBase      : "${libraryArtifactBaseId}-component-base:${versions.library}@aar",
        componentCommon    : "${libraryArtifactBaseId}-component-common:${versions.library}@aar",
        componentGroup     : "${libraryArtifactBaseId}-component-group:${versions.library}@aar",
        componentEditLayout: "${libraryArtifactBaseId}-component-editlayout:${versions.library}@aar"
]

def androidx = [
        annotation    : [annotation: "androidx.annotation:annotation:1.1.0"],
        appcompat     : [appcompat: "androidx.appcompat:appcompat:1.1.0"],
        core          : [core: "androidx.core:core:1.2.0"],
        drawerlayout  : [drawerlayout: "androidx.drawerlayout:drawerlayout:1.0.0"],
        legacy        : [
                coreUi   : "androidx.legacy:legacy-support-core-ui:1.0.0",
                coreUtils: "androidx.legacy:legacy-support-core-utils:1.0.0",
                supportV4: "androidx.legacy:legacy-support-v4:1.0.0"
        ],
        recyclerview  : [recyclerview: "androidx.recyclerview:recyclerview:1.1.0"],
        test          : [
                core    : "androidx.test:core:1.2.0",
                espresso: [
                        contrib: "androidx.test.espresso:espresso-contrib:${versions.androidx.test.espresso}",
                        core   : "androidx.test.espresso:espresso-core:${versions.androidx.test.espresso}",
                        intents: "androidx.test.espresso:espresso-intents:${versions.androidx.test.espresso}"
                ],
                ext     : [junit: "androidx.test.ext:junit:1.1.1"],
                rules   : "androidx.test:rules:1.2.0",
                runner  : "androidx.test:runner:1.2.0"
        ],
        vectordrawable: [vectordrawable: "androidx.vectordrawable:vectordrawable:1.1.0"]
]

def google = [android: [material: [material: "com.google.android.material:material:1.1.0"]]]

def test = [
        junit      : "junit:junit:${versions.test.junit}",
        mockito    : [
                core   : "org.mockito:mockito-core:${versions.test.mockito}",
                android: "org.mockito:mockito-android:${versions.test.mockito}"
        ],
        robolectric: [
                robolectric     : "org.robolectric:robolectric:${versions.test.robolectric}",
                shadowsMultidex : "org.robolectric:shadows-multidex:${versions.test.robolectric}",
                shadowsSupportV4: "org.robolectric:shadows-supportv4:${versions.test.robolectric}"
        ]
]

def universum = [studios: [
        font          : [core: 'universum.studios.android:font-core:1.2.0@aar'],
        fragments     : 'universum.studios.android:fragments:1.4.3@aar',
        samples       : 'universum.studios.android:samples:0.2.0@aar',
        testing       : [
                testing : 'universum.studios.android:testing:1.0.0@aar',
                activity: 'universum.studios.android:testing-activity:1.0.0@aar',
                view    : 'universum.studios.android:testing-view:1.0.0@aar'
        ],
        ui            : 'universum.studios.android:ui:0.10.3@aar',
        widgetAdapters: 'universum.studios.android:widget-adapters:2.1.2@aar'
]]

def other = [
        butterknife: [
                butterknife: "com.jakewharton:butterknife:${versions.butterknife}",
                processor  : "com.jakewharton:butterknife-compiler:${versions.butterknife}"
        ]
]

ext.versions = versions
ext.deps = [
        "library"  : library,
        "androidx" : androidx,
        "google"   : google,
        "test"     : test,
        "universum": universum,
        "other"    : other
]