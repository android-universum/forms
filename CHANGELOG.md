Change-Log
===============
> Regular configuration update: _01.09.2019_

## Version 1.x ##

### 1.1.1 ###
> 07.02.2019

- Re-deploy for version **1.1.0** due to missing **aar** artifacts.

### 1.1.0 ###
> 12.01.2019

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).

### 1.0.1 ###
> 05.06.2018

- Small updates and improvements.

### 1.0.0 ###
> 03.08.2017

- First production release.
- **Dropped support** for _Android_ versions **below** _API Level 14_.

### 1.0.0-beta2 ###
> 27.01.2017

- Library divided into separate modules.

### 1.0.0-beta1 ###
> 24.01.2017

- First beta release.