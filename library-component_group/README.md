@Forms-Component
===============

This module groups the following modules into one **single group**:

- [Component-Base](https://bitbucket.org/android-universum/forms/src/main/library-component-base)
- [Component-Common](https://bitbucket.org/android-universum/forms/src/main/library-component-common)
- [Component-EditLayout](https://bitbucket.org/android-universum/forms/src/main/library-component-editlayout)
- [Component-Group](https://bitbucket.org/android-universum/forms/src/main/library-component-group)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aforms/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aforms/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:forms-component:${DESIRED_VERSION}@aar"

_depends on:_
[forms-core](https://bitbucket.org/android-universum/forms/src/main/library-core),
[forms-component-base](https://bitbucket.org/android-universum/forms/src/main/library-component-base)