/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.form;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.SparseArray;
import android.view.View;

/**
 * Groups a set of {@link FormComponent FormComponents} into one group which can be validated.
 * There is only one difference between group of all components within {@link FormValidator} and
 * group of components here. If during validation process there is found an invalid component
 * ({@link FormComponent#validate()} of one of group components returns {@code false}), validation
 * of such group stops and no next form components from that group are validated.
 * <p>
 * Invalid form component for the current validation process can be obtained via {@link #getInvalidComponent()}.
 * This can be useful, when there are components in form, where some of them depends on each other
 * and if some of these dependent components is invalid, there is no need to check next ones.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
public class FormComponentsGroup extends BaseFormComponent<View, Void> {

	/*
	 * Interface ===================================================================================
	 */

	/*
	 * Constants ===================================================================================
	 */

	/**
	 * Log TAG.
	 */
	// private static final String TAG = "FormComponentsGroup";

	/*
	 * Static members ==============================================================================
	 */

	/*
	 * Members =====================================================================================
	 */

	/**
	 * Set of FormComponents presented within this group.
	 */
	private final SparseArray<FormComponent> components = new SparseArray<>();

	/**
	 * Invalid component for the current validation process. Should be reset when a new validation is
	 * requested.
	 */
	private FormComponent invalidComponent;

	/**
	 * Current size of this group. Mirrored size of {@link #components}.
	 */
	private int size;

	/*
	 * Constructors ================================================================================
	 */

	/**
	 * Creates a new instance of FormComponentsGroup to
	 *
	 * @param id The id representing this group of form components.
	 */
	public FormComponentsGroup(final int id) {
		super(id, null);
	}

	/*
	 * Methods =====================================================================================
	 */

	/**
	 * Checks whether the given <var>component</var> represents group of form components or not.
	 *
	 * @param component An instance of form component to check for group.
	 * @return {@code True} if the given component is group, {@code false} otherwise.
	 */
	public static boolean isGroup(@NonNull final FormComponent component) {
		return component instanceof FormComponentsGroup;
	}

	/**
	 */
	@Override public void attachToValidator(@NonNull final FormValidator validator) {
		super.attachToValidator(validator);
		for (int i = 0; i < size; i++) {
			components.get(components.keyAt(i)).attachToValidator(validator);
		}
	}

	/**
	 */
	@Override public boolean hasInputChanged() {
		for (int i = 0; i < size; i++) {
			if (components.get(i).hasInputChanged()) return true;
		}
		return false;
	}

	/**
	 */
	@Override public boolean validate() {
		this.setInvalidComponent(null);
		return onValidate(null);
	}

	/**
	 */
	@Override protected boolean onValidate(@Nullable final Void input) {
		for (int i = 0; i < size; i++) {
			final FormComponent component = components.get(components.keyAt(i));
			if (!component.validate()) {
				this.setInvalidComponent(component);
				return false;
			}
		}
		return true;
	}

	/**
	 * Adds the given form components into this group. See {@link FormComponentsGroup} class description
	 * for info about how group of form components behave during validation.
	 *
	 * @param components Form components which should be part of this group.
	 * @return This group to allow methods chaining.
	 */
	public FormComponentsGroup addComponents(@NonNull final FormComponent... components) {
		if (components.length > 0) {
			for (final FormComponent component : components) {
				addComponent(component);
			}
		}
		return this;
	}

	/**
	 * Adds the given form component into this group. See {@link FormComponentsGroup} class description
	 * for info about how group of form components behave during validation.
	 *
	 * @param component Form component which should be part of this group.
	 * @return This group to allow methods chaining.
	 */
	public FormComponentsGroup addComponent(@NonNull final FormComponent component) {
		final int componentId = component.getId();
		if (components.indexOfKey(componentId) < 0) {
			components.put(componentId, component);
			this.size = components.size();
		}
		return this;
	}

	/**
	 * Always returns {@code null}, as a group of form components does not have a specific view attached.
	 */
	@Override @Nullable public View getView() {
		return null;
	}

	/**
	 * Always returns {@code null}, as a group of form components does not have a specific view attached
	 * thus it cannot have any input also.
	 */
	@Override @NonNull public Void getInput() {
		return null;
	}

	/**
	 * Returns all components from this group.
	 *
	 * @return The current set of form components.
	 */
	@NonNull public SparseArray<FormComponent> getComponents() {
		return components;
	}

	/**
	 * Returns an instance of FormComponent with the requested id. Unlike {@link #findComponent(int)},
	 * this will check only at the top of the current components hierarchy.
	 *
	 * @param componentId Id of the desired form component to obtain.
	 * @return Instance of requested form component with the specified id or {@code null} if there
	 * is no such a form component presented within this group.
	 */
	@Nullable public FormComponent getComponent(final int componentId) {
		return components.size() > 0 ? components.get(componentId) : null;
	}

	/**
	 * Returns currently invalid form component.
	 *
	 * @return An instance of FormComponent which returns {@code false} from {@link BaseFormComponent#validate()}
	 * during the last validation process, or {@code null} if there was no validation requested
	 * upon this group yet, or the last validation was successful.
	 */
	@Nullable public FormComponent getInvalidComponent() {
		return invalidComponent;
	}

	/**
	 * This implementation does nothing.
	 */
	@Override public void setError(@NonNull final CharSequence errorMessage) {
		// The components group cannot possibly know to which component it should set the error.
	}

	/**
	 * Clears the current errors of all components within this group.
	 */
	@Override public void clearError() {
		super.clearError();
		for (int i = 0; i < size; i++) {
			components.get(components.keyAt(i)).clearError();
		}
	}

	/**
	 * Will search within the current form components to find a one with the requested <var>id</var>.
	 *
	 * @return An instance of requested component (can be also this group), {@code null} if
	 * this group does not contain form component with such an <var>id</var>.
	 */
	@Override @Nullable protected FormComponent findComponentTraversal(final int id) {
		if (this.id == id) {
			return this;
		}
		for (int i = 0; i < size; i++) {
			final FormComponent component = components.get(components.keyAt(i)).findComponent(id);
			if (component != null) {
				return component;
			}
		}
		return null;
	}

	/**
	 * Sets the currently invalid form component.
	 *
	 * @param component An instance of form component to set as invalid for the current validation
	 *                  process.
	 */
	private void setInvalidComponent(final FormComponent component) {
		this.invalidComponent = component;
	}

	/*
	 * Inner classes ===============================================================================
	 */
}